import Storage from '../src/storage'
import s3Lister from 's3-lister'
import ps from 'promise-streams'
import Debug from 'debug'
import co from 'co'
import yargs from 'yargs'
import Queue from 'bull'

let { argv } = yargs
let host = yargs.argv.host || 'localhost'
const debug = Debug('storage')
let storage = new Storage()
let imgStorage = new Storage('cadimg')
let Q = Queue('img', 6379, host)
debug(host)
function copyImgs(job, cb){
let { filename } = job.data
//debug(`copying ${filename}`)
storage.copyTo(filename, 'cadimg', filename)
	.end(()=>{
		storage.del(filename).end(()=>{
			debug(`done: ${filename}`)
			cb()
		})
	})
}
function Lister(){
	let lister = new s3Lister(storage)
		
	lister.on('data', data=>{
	  if(!data.Key.match(/\.jpg$/))return;
	  debug(`add key: ${data.Key}`)
	  Q.add({filename: data.Key})
	})
	lister.on('end', ()=>{
	  process.exit()
	})
	return lister
}

if(argv.start){
	Lister()
}
else{
	Q.process(copyImgs)
}
