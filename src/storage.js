import Debug from 'debug'
import _ from 'lodash'
import S3 from './co-knox'
import mixins from 'es6-mixins'
import buffer from 'stream-buffer'
import ps from 'promise-streams'

const debug = Debug('storage')

let config = {
  key: 'AKIAJ5INHVZODJ5STMOQ'
  , secret: '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'
  , bucket: 'cadq'
}

export default class Storage {
  constructor(bucket){
    config.bucket = bucket || 'cadq'
    let s3 = S3(config)
    if(!s3.putStreamBuffer)
    mixins(StorageMixin, s3)
    return s3
  }
}
class StorageMixin {
   *putStreamBuffer(Stream, filename, header){
    let ctx = this
    let _buffer = buffer()
    return new Promise((resolve, reject)=>{
      ps.wait(Stream.pipe(_buffer))
      .then(()=>{
        let length = 0
        _buffer.buffer.forEach(b=>{
          length+=b.length
        })
        header['Content-Length'] = length
        return ps.wait(_buffer.replay(ctx.put(filename, header)))
      })
      .then(()=>{
        return resolve()
      })
      .catch(err=>{
        return reject(err)
      })
    })
   }
}