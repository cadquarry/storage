import Debug from 'debug'
import { expect } from 'chai'
import Storage from './storage.js'
import _ from 'lodash'
import downloadStream from 'download-stream'
import foreach from 'generator-foreach'
import denodeify from 'es6-denodeify'
import request from 'superagent'
import stdout from 'stdout'
import ps from 'promise-streams'
import buffer from 'stream-buffer'
import ms from 'ms'

let debug = Debug('storage')

let url = 'https://github.com/Automattic/knox/blob/088050868e59ec9bf5bc5fc91d606c37ca0c78d5/test/knox.test.js'
describe('Storage',function(){
  this.timeout(1000*60*2)
  it('should put a file', function(){
    let fileName = 'test.js'
    let storage = new Storage()
    let req = request.get(url)

    let header = {
      'Content-Length':1000
      , 'Content-Type':'text/plain'
    }
    // req.on('end', done)
    let _buffer = buffer()
    return ps.wait(req.pipe(_buffer))
    .then(()=>{
      let length = 0
      _buffer.buffer.forEach(b=>{
        length+=b.length
      })
      header['Content-Length'] = length
      let put = storage.put(fileName, header)
      return ps.wait(_buffer.replay(put))
    })
    .then(()=>{
      return storage.del(fileName).end()
    })
    
  })
  it('should put a stream via buffer', function*(){
    let fileName = 'test.js'
    let storage = new Storage()
    let req = request.get(url)
    let header = {}
    yield storage.putStreamBuffer(req, fileName, header)
    return storage.del(fileName).end()
  })
  it('should put zip file stream via buffer', function*(){
    let fileName = 'test.zip'
    let storage = new Storage()
    let req = request.get('http://dl09.bibliocad.com/download/143465402015061819/88890_telephone-network.zip')
    let header = {
      'Content-Type':'application/zip'
    }
    yield storage.putStreamBuffer(req, fileName, header)
    return storage.del(fileName).end()
  })
  it('should put zip file stream via buffer', function*(){
    let fileName = 'test.zip'
    let storage = new Storage()
    let req = request.get('http://dl09.bibliocad.com/download/143465402015061819/88890_telephone-network.zip')
    let header = {
      'Content-Type':'application/zip'
    }
    yield storage.putStreamBuffer(req, fileName, header)
    return storage.del(fileName).end()
  })
})
describe('getFileByName', function(){
  this.timeout(1000*60*2)
  let storage = new Storage()
  let fileName = '/test.zip'
  before(function*(){
    let req = request.get('http://dl08.bibliocad.com/download/143465942015061820/88415_calculation-submersible-pump.zip')
    let header = {
      'Content-Type':'application/zip'
    }
    yield storage.putStreamBuffer(req, fileName, header)
  })
  after(function(){
    return storage.del(fileName).end()
  })
  it.only('should get file', function(done){
    storage.getFile(fileName, function(){
      debug(arguments)
      done()
    })
    return
    let file = storage.get(fileName).end()
    debug(file)
    return
    file.on('error', err=>{
      debug(err)
    })
    debug(file)
    // debug(file)
    return ps.wait(file.pipe(stdout()))
  })
})
describe('getFileByName', function(){
  this.timeout(1000*60*2)
  let storage = new Storage()
  let fileName = '/test.zip'
  before(function*(){
    let req = request.get('http://dl08.bibliocad.com/download/143465942015061820/88415_calculation-submersible-pump.zip')
    let header = {
      'Content-Type':'application/zip'
    }
    yield storage.putStreamBuffer(req, fileName, header)
  })
  after(function(){
    return storage.del(fileName).end()
  })
  it('should get file', function(){
    let file = storage.get(fileName)
    file.on('error', err=>{
      debug(err)
    })
    debug(file)
    return ps.wait(file.pipe(stdout()))
  })
})
describe('signedUrl', function(){
  this.timeout(1000*60*2)
  let storage = new Storage()
  let fileName = '/test.zip'
  before(function*(){
    let req = request.get('http://dl08.bibliocad.com/download/143465942015061820/88415_calculation-submersible-pump.zip')
    let header = {
      'Content-Type':'application/zip'
    }
    yield storage.putStreamBuffer(req, fileName, header)
  })
  after(function(){
    return storage.del(fileName).end()
  })
  it.only('should get signed url', function(){
    let url = storage.signedUrl(fileName, new Date(Date.now()+ms('5m')))
    debug(url)
  })
})