import Knox from 'knox'
import thunkify from 'thunkify'
import Debug from 'debug'
import _ from 'lodash'
let debug = Debug('storage')
let requestFunctions = [
  'del'
]
export default (options)=>{
  return wrap(Knox.createClient(options))
} 

function wrap(obj) {
  // return obj
  Object.keys(obj.__proto__).forEach(function(key) {
    if('function' != typeof obj.__proto__[key]) return;
    if(_.indexOf(requestFunctions, key)>-1){
      obj[key] = promisifyRequest(obj[key])
    }
    // obj[key] = thunkify(obj[key]);
  });
  return obj;
}

function promisifyRequest(func){
  return func
}
