#Usage
```js
import Storage from 'storage'
let storage = Storage()

storage.get('/filename')
.pipe(...)
```