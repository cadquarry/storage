import Storage from '../src/storage'
import s3Lister from 's3-lister'
import ps from 'promise-streams'
import Debug from 'debug'

const debug = Debug('storage')
let storage = new Storage()
let imgStorage = new Storage('cadimg')

let lister = new s3Lister(storage)

lister.on('data', data=>{
  if(!data.Key.match(/\.jpg$/))return;
  return ps.wait(storage.copyTo(data.Key, 'cadimg', data.Key))
  .then(()=>{
    debug(`Done moving: ${data.Key}`)
  })
})
lister.on('end', ()=>{
  process.exit()
})
